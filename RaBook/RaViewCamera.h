//
//  RaViewCamera.h
//  RaBook
//
//  Created by Leandro Fantin on 6/8/16.
//  Copyright © 2016 com.piantao. All rights reserved.
//

#import <opencv2/videoio/cap_ios.h>

@protocol RaViewCameraDelegateMod <CvVideoCameraDelegate>
@end

@interface RaViewCamera : CvVideoCamera

- (void)updateOrientation;
- (void)layoutPreviewLayer;

@property (nonatomic, strong) CALayer *customPreviewLayer;

@end
