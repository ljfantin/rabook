//
//  Stats.hpp
//  RaBook
//
//  Created by Leandro Fantin on 13/8/16.
//  Copyright © 2016 com.piantao. All rights reserved.
//

#ifndef Stats_hpp
#define Stats_hpp

#include <stdio.h>

struct Stats
{
    int matches;
    int inliers;
    double ratio;
    int keypoints;
    
    Stats() : matches(0),
    inliers(0),
    ratio(0),
    keypoints(0)
    {}
    
    Stats& operator+=(const Stats& op) {
        matches += op.matches;
        inliers += op.inliers;
        ratio += op.ratio;
        keypoints += op.keypoints;
        return *this;
    }
    Stats& operator/=(int num)
    {
        matches /= num;
        inliers /= num;
        ratio /= num;
        keypoints /= num;
        return *this;
    }
};

#endif /* Stats_hpp */
