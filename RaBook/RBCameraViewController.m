//
//  RBCameraViewController.m
//  RaBook
//
//  Created by Leandro Fantin on 1/8/16.
//  Copyright © 2016 com.piantao. All rights reserved.
//

#import "RBCameraViewController.h"
#import <opencv2/videoio/cap_ios.h>
#include "opencv2/core.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/calib3d.hpp"
#include "RaViewCamera.h"
#include <vector>
#import "UIImageUtils.h"
#include "Stats.hpp"

static NSString *const kRBCameraViewControllerMachesCountLabel = @"Maches: %@";
static NSString *const kRBCameraViewControllerInlierCountLabel = @"Inlier: %@";
static NSString *const kRBCameraViewControllerRatioLabel = @"Ratio: %@";
static NSString *const kRBCameraViewControllerKeypointsLabel = @"keypoints: %@";


using namespace cv;

@interface RBCameraViewController () <CvVideoCameraDelegate>
{
    cv::Mat takenImage;
    Mat *localDescriptors;
    
    Mat localImageDescriptors;
    std::vector<KeyPoint> localKeyPoints;
    Mat matLocalImage;
}

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (strong, nonatomic) CvVideoCamera* videoCamera;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
// Cola serial, para hacer el procesamiento de la imagen
@property (strong, nonatomic) dispatch_queue_t serialQueue;
@property (weak, nonatomic) IBOutlet UIButton *startButton;

// labels
@property (weak, nonatomic) IBOutlet UILabel *matchesLabel;
@property (weak, nonatomic) IBOutlet UILabel *inliersLabel;
@property (weak, nonatomic) IBOutlet UILabel *ratioLabel;
@property (weak, nonatomic) IBOutlet UILabel *keypointsLabel;



@property (assign, nonatomic, getter=isScanning) BOOL scanning;


@end

@implementation RBCameraViewController


- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        localDescriptors = new Mat();
        // Inicializo una cola para el procesamiento de la respuesta de las celdas
        self.serialQueue = dispatch_queue_create("com.piantao.rabook-proccess", DISPATCH_QUEUE_SERIAL);
    }
    return self;
}

- (void)dealloc
{
    delete localDescriptors;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.title = @"Scan picture";
    [self setupStaticsLayer];

    self.videoCamera = [[CvVideoCamera alloc] initWithParentView:self.imageView];
    self.videoCamera.defaultAVCaptureDevicePosition = AVCaptureDevicePositionBack;
    self.videoCamera.defaultAVCaptureSessionPreset = AVCaptureSessionPresetHigh;
    self.videoCamera.defaultAVCaptureVideoOrientation =  AVCaptureVideoOrientationPortrait;

    self.videoCamera.defaultFPS = 30;
    self.videoCamera.delegate = self;
    self.videoCamera.recordVideo = NO;
    self.videoCamera.grayscaleMode = NO;
    self.activityIndicator.alpha = 0;
    
    __weak typeof(self) weakSelf = self;
    self.startButton.userInteractionEnabled = NO;

    [self showLoadingMessageAnimatedWithCompletion:^{
        dispatch_async(self.serialQueue, ^{
            matLocalImage = [UIImageUtils cvMatFromUIImage:[UIImage imageNamed:@"yoda"]];
            [weakSelf detectAndCompute:matLocalImage descriptors:localImageDescriptors keyPoints:localKeyPoints];
            dispatch_async(dispatch_get_main_queue(), ^{

                [weakSelf hideLoadingMessageAnimatedWithCompletion:^{
                    weakSelf.startButton.userInteractionEnabled = YES;
                }];
            });
        });
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSLog(@"capture session loaded: %d", [self.videoCamera captureSessionLoaded]);
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self.videoCamera stop];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.videoCamera start];
}


- (IBAction)buttonStartDidTouch:(id)sender
{
    self.scanning = YES;
}

- (void)setupStaticsLayer
{
}

- (void)processImage:(cv::Mat &)frameImage outputImage:(cv::Mat &)output stats:(Stats &)stats
{
    Mat frameImageDescriptors;
    std::vector<KeyPoint> frameImageKeyPoints;
    [self detectAndCompute:frameImage descriptors:frameImageDescriptors keyPoints:frameImageKeyPoints];
    
    BFMatcher matcher(NORM_HAMMING);
    std::vector< std::vector<DMatch> > nn_matches;
    matcher.knnMatch(frameImageDescriptors, localImageDescriptors, nn_matches, 2);
    
    const float nn_match_ratio = 0.75f;
    std::vector<KeyPoint> localImageMatched, frameImageMatched, inliers1, inliers2;
    std::vector<DMatch> good_matches;
    std::vector< Point2f > obj;
    std::vector< Point2f > scene;
    for(size_t i = 0; i < nn_matches.size(); i++) {
        DMatch first = nn_matches[i][0];
        float dist1 = nn_matches[i][0].distance;
        float dist2 = nn_matches[i][1].distance;
        
        if(dist1 < nn_match_ratio * dist2) {
            localImageMatched.push_back(localKeyPoints[first.queryIdx]);
            frameImageMatched.push_back(frameImageKeyPoints[first.trainIdx]);
            obj.push_back(localKeyPoints[first.queryIdx].pt);
            scene.push_back(frameImageKeyPoints[first.trainIdx].pt);
        }
    }
    NSLog(@"local image matched %ld", localImageMatched.size());
    if (localImageMatched.size() < 10) {
        return;
    }
    
    const double ransac_thresh = 3;
    Mat inlier_mask, homography;
    homography = findHomography(obj, scene, RANSAC, ransac_thresh, inlier_mask);
        
    if (homography.empty()) {
        return;
    }
    
    std::vector<DMatch> inlier_matches;
    for(unsigned i = 0; i < localImageMatched.size(); i++) {
        if(inlier_mask.at<uchar>(i)) {
            int new_i = static_cast<int>(inliers1.size());
            inliers1.push_back(localImageMatched[i]);
            inliers2.push_back(frameImageMatched[i]);
            inlier_matches.push_back(DMatch(new_i, new_i, 0));
        }
    }
    
    //drawPerspective(output, matLocalImage, obj, scene);
    //drawBoundingBox(output,scene);
    //[self drawregion:matLocalImage homography:homography img:output];
    
    Point2f center;
    float radius;
    std::vector<Point2f> objWithTransform(4);
    perspectiveTransform( obj, objWithTransform, homography);
    minEnclosingCircle(objWithTransform,center,radius);
    circle(output,center,radius,Scalar(0,255,0),4);
    
    // stats
    stats.inliers = (int)inlier_matches.size();
    stats.keypoints = (int)localKeyPoints.size();
    stats.matches = (int)localImageMatched.size();
    stats.ratio = nn_match_ratio;
}

- (void)drawregion:(cv::Mat &)img_object homography:(cv::Mat)H img:(cv::Mat &)img_matches
{
    std::vector<Point2f> obj_corners(4);
    obj_corners[0] = cvPoint(0,0); obj_corners[1] = cvPoint( img_object.cols, 0 );
    obj_corners[2] = cvPoint( img_object.cols, img_object.rows ); obj_corners[3] = cvPoint( 0, img_object.rows );
    std::vector<Point2f> scene_corners(4);
    
    perspectiveTransform( obj_corners, scene_corners, H);
    
    line( img_matches, scene_corners[0] + Point2f( img_object.cols, 0), scene_corners[1] + Point2f( img_object.cols, 0), Scalar(0, 255, 0), 4 );
    line( img_matches, scene_corners[1] + Point2f( img_object.cols, 0), scene_corners[2] + Point2f( img_object.cols, 0), Scalar( 0, 255, 0), 4 );
    line( img_matches, scene_corners[2] + Point2f( img_object.cols, 0), scene_corners[3] + Point2f( img_object.cols, 0), Scalar( 0, 255, 0), 4 );
    line( img_matches, scene_corners[3] + Point2f( img_object.cols, 0), scene_corners[0] + Point2f( img_object.cols, 0), Scalar( 0, 255, 0), 4 );
}

void drawBoundingBox(Mat image, std::vector<Point2f> bb)
{
    for(unsigned i = 0; i < bb.size() - 1; i++) {
        line(image, bb[i], bb[i + 1], Scalar(0, 0, 255), 2);
    }
    line(image, bb[bb.size() - 1], bb[0], Scalar(0, 0, 255), 2);
}

void drawPerspective(cv::Mat& image, // output image
                                            cv::Mat& image1, // image 1
                                            std::vector<cv::Point2f>& points1, // keypoints 1
                                            std::vector<cv::Point2f>& points2) // keypoints 2
{
    cv::Mat H = cv::findHomography( points1, points2, cv::RANSAC );
    if( !H.empty() )
    {
        // get the corners from the object image
        std::vector<cv::Point2f> obj_corners(4), scene_corners(4);
        
        obj_corners[0] = cv::Point2f(0,0);
        obj_corners[1] = cv::Point2f( image1.cols, 0 );
        obj_corners[2] = cv::Point2f( image1.cols, image1.rows );
        obj_corners[3] = cv::Point2f( 0, image1.rows );
        
        // calculate perspective transformation of object corners within scene
        cv::perspectiveTransform( obj_corners, scene_corners, H);
        
        // draw lines between the corners (the mapped object in the scene)
        cv::line( image, scene_corners[0], scene_corners[1], cv::Scalar( 0, 0, 255 ), 4 );
        cv::line( image, scene_corners[1], scene_corners[2], cv::Scalar( 0, 0, 255 ), 4 );
        cv::line( image, scene_corners[2], scene_corners[3], cv::Scalar( 0, 0, 255 ), 4 );
        cv::line( image, scene_corners[3], scene_corners[0], cv::Scalar( 0, 0, 255 ), 4 );
    }
}

#pragma mark - CvVideoCameraDelegate
- (void)processImage:(cv::Mat&)image
{
  //  takenImage = image.clone();
    
    if (self.isScanning) {
        cv::Mat inputFrame = image.clone();
    //    cv::Mat outputFrame = image;
        Stats stats;
        [self processImage:inputFrame outputImage:image stats:stats];
        __weak typeof(self) weakSelf = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf showStaticsWithStats:stats];
        });
    }
}


- (void)detectAndCompute:(cv::Mat &)imageIn descriptors:(OutputArray)descriptors keyPoints:(std::vector<KeyPoint> &)keyPoints
{
    cvtColor(imageIn, imageIn, COLOR_RGB2GRAY);
    // obtenemos mayor contraste
    //equalizeHist( imageIn, imageIn );
   Ptr<ORB> orb = ORB::create();
   orb->detectAndCompute(imageIn, noArray(), keyPoints, descriptors);
    
   // Ptr<BRISK> brisk = BRISK::create();
   // brisk->detectAndCompute(imageIn, noArray(), keyPoints, descriptors);

}

- (void)showStaticsWithStats:(const Stats &)stats
{
    self.matchesLabel.alpha = 1;
    self.matchesLabel.text = [NSString stringWithFormat:kRBCameraViewControllerMachesCountLabel, @(stats.matches).stringValue];
    self.inliersLabel.alpha = 1;
    self.inliersLabel.text = [NSString stringWithFormat:kRBCameraViewControllerInlierCountLabel,@(stats.inliers).stringValue];
    self.ratioLabel.alpha = 1;
    self.ratioLabel.text = [NSString stringWithFormat:kRBCameraViewControllerRatioLabel,@(stats.ratio).stringValue];
    self.keypointsLabel.alpha = 1;
    self.keypointsLabel.text = [NSString stringWithFormat:kRBCameraViewControllerKeypointsLabel,@(stats.keypoints).stringValue];
}

- (void)hideStatics
{
}

- (void)showLoadingMessageAnimatedWithCompletion:(void (^)(void))completion
{
    __weak typeof(self) weakSelf = self;
    
    [UIView animateWithDuration:0.2 delay:0 options:(UIViewAnimationOptionCurveEaseIn | UIViewAnimationOptionBeginFromCurrentState) animations: ^{
        weakSelf.activityIndicator.alpha = 1.0f;
    } completion:^(BOOL finished) {
        if (completion != nil) {
            completion();
        }
    }];
}

- (void)hideLoadingMessageAnimatedWithCompletion:(void (^)(void))completion
{
    __weak typeof(self) weakSelf = self;
    
    [UIView animateWithDuration:0.2 delay:0 options:(UIViewAnimationOptionCurveEaseIn | UIViewAnimationOptionBeginFromCurrentState) animations: ^{
        weakSelf.activityIndicator.alpha = 0.0f;
    } completion:^(BOOL finished) {
        if (completion != nil) {
            completion();
        }
    }];
}

@end
