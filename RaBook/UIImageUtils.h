//
//  UIImageUtils.h
//  RaBook
//
//  Created by Leandro Fantin on 7/8/16.
//  Copyright © 2016 com.piantao. All rights reserved.
//

#include "opencv2/core.hpp"
#import <UIKit/UIKit.h>

@interface UIImageUtils : NSObject

+ (cv::Mat)cvMatFromUIImage:(UIImage *)image;
+ (UIImage *)UIImageFromCVMat:(cv::Mat)cvMat;

@end
